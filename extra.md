<!--v-->

# Challenges


## Hardware accessibility and durability

* Closed-source hardware tend to be abandoned at some point
* Need to follow the trend, as for smartphones, to benefit from AR/VR new experiences
* Open source hardware (OSH) is not up to par, especially regarding availability
* Also for the public, OSH does not mean accessible or necessarily more durable (question of availability of resources to sustain the ecosystem)

<!--v-->
#  On the software side, tendency to build walled garden

* See Meta Horizon, Fortnite, Roblox, AR${something-forgot-the-name}...
* Overall, platforms not meant to be communicate with each other
* Recent history (Twitter) has shown the dangers with centralized spaces, especially for marginalized communities
* Go for decentralized platforms?
  * VR community over Matrix