---
title: Challenges and Opportunities for Equity Diversity and Inclusion in Immersive Arts
subtitle: 2023 Workshop on Inclusion in Virtual Reality
author: Christian Frisson
institute: Société des Arts Technologiques [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: white 
revealOptions:
  transition: 'none' 
  loop: false 
  slideNumber: "c/t"
  autoPlayMedia: true
--- 

<!-- .slide: id="start" -->
  <h2 data-i18n="title">Challenges and Opportunities for Equity Diversity and Inclusion in Immersive Arts</h2>
  <h3 data-i18n="conference"><a href="https://ivr2023.squarespace.com/">2023 Workshop on Inclusion in Virtual Reality</a></h3>
  <h4 data-i18n="subtitle">2023-03-06</h4>

  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name">
      <a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
      </div>
    </div>
    <div class="author">
      <img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
      <div class="name">
      <a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
      </div>
    </div>
  </div>


<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/1-LOGO_SAT_noir_sans_texte.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)

<!--s-->
  <!-- .slide: id="sat" -->
  <h2 data-i18n="title"><a href="https://sat.qc.ca">Society for Arts and Technology</a></h2>

<div class="row">
<div class="col-20 fragment">
  <img src="images/sat.svg" style="height:700px;margin-top:-50px" />  
</div>
<div class="col-80 fragment">

<iframe src="https://player.vimeo.com/video/390637480?loop=false&amp;autoplay=false&amp;muted=false&amp;gesture=media&amp;playsinline=true&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=false&amp;customControls=true" allowfullscreen="" allow="autoplay; fullscreen; picture-in-picture; encrypted-media; accelerometer; gyroscope" width="640" height="360" title="Player for Société des arts technologiques (SAT)" data-ready="true" tabindex="-1"></iframe>

</div>
</div>

Notes:

> In the heart of Montreal, the SAT is a unique place that offers the public immersive experiences in its famous dome, but also concerts, workshops, conferences, exhibitions… Hundreds of events are presented there every year.

> Founded in 1996, the Society for Arts and Technology [SAT] is a non-profit organization dedicated to digital culture. With its triple mission as a center for the arts, training and research, the SAT is a gathering space for diverse talent, curiosity, and knowledge. It is recognized internationally for its active, leading role in developing technologies for immersive creation, mixed realities and telepresence. The Society for Arts and Technology is a place of collective learning that holds the promise of exploring technology to infuse it with more meaning, magic and humanity. 

<!--v-->
  <!-- .slide: id="livepose-experimentations" -->
  <h2 data-i18n="title">Immersive Arts</h2>
  <iframe src="https://player.vimeo.com/video/639532760"  data-audio-controls  width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--v-->

# Directions for Innovation

<div class="row">

<div class="col-50">

Left direction

<div class="figure">
<img class="thumb" alt="Facebook CEO Mark Zuckerberg (left) walks past a crowd with virtual reality (VR) headsets" src="images/3rdparty/zuckerberg-vr-crowd.jpg"/>
<!-- <div class="cite"> -->

https://www.businessinsider.com/metaverse-facebook-hiring-10000-people-to-build-mark-zuckerbergs-project-2021-10

<!-- </div> -->
</div>
</div>

<div class="col-50 fragment">

Right direction

<div class="figure">
<img class="thumb" alt="" src="images/3rdparty/BretVictor/SeeingSpaces.jpg"/>
<!-- <div class="cite"> -->

http://worrydream.com/#!/SeeingSpaces

<!-- </div> -->
</div>

</div>
</div>

Notes:
* Equip/Instrument spaces rather than people
* Do not rely on hardware brought by people for them to live the experience
* A tool for inclusivity

<!--s-->

# Challenges

<!--v-->
# Democratizing camera-based group interaction

<div class="row">
<div class="col-50">

![](images/projects/MusicmotionHacklab2021/_mg_0123_sebastienroy.jpg)

Team: Percepto

</div>
<div class="col-50">

![](images/projects/MusicmotionHacklab2021/_mg_0382_sebastienroy.jpg)

Team: Le chant du canevas

</div>

MusicMotion Hacklab 2021 with/at the SAT, Digital Arts and Inclusion

Photos: Sébastien Roy

https://sat.qc.ca/fr/nouvelles/hacklab2021

</div>

Notes:
* Allow for everybody to be part of the experience
* Enable detection over large spaces
* A tool for bringing interactivity into the physical space
* Not a tool for surveillance!
* Challenge of being able to estimate poses and action for anybody, regardless of their specificity

<!--v-->
# Communicating using only body language

![images/projects/Satellite/the_mutual_penetration_of_physical_and_virtual_bodies.png](images/projects/Satellite/the_mutual_penetration_of_physical_and_virtual_bodies.png)

Ἐphemera One (Daria Smakhtina – Vadim Smakhtina) (Ru). 
The Mutual Penetration Of Physical And Virtual Bodies. 
Satellite Campus SAT VR/AR. 
Real/Virtual Experience in Satellite / Mozilla Hubs

https://sat.qc.ca/en/satellite-virtual-space - https://satellite.sat.qc.ca/ktWzSMr/

Notes:
> The installation is an artistic investigation of emerging forms of virtual and physical communications developed through Mozilla’s custom Hubs platforms. It is an interactive environment intended for two users to communicate using only with body language. The first user uses a virtual reality headset and is represented by an avatar. The avatar is located in a space, which is entirely controlled by the second user. By employing their body language, the second user changes the appearance of the room, which thus serves as a device for spatial communication, replacing spoken language with a language of non-orthogonal shapes.
* Avatar: representation of a white male

<!--s-->
# Opportunities

<!--v-->
# Training systems for accessibility, diversity, inclusion

<div class="row">
<div class="col-50 fragment">

<iframe width="560" height="315" src="https://www.youtube.com/embed/-GVW2C2MXas" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

https://creo.ca/en/megaceta-2/

</div>
<div class="col-50 fragment">

![](images/projects/CREO/2022-CREO-Megaceta_i3d_scratch_matrix_image-2023-01-20.png)

https://github.com/open-mmlab/mmaction2

</div>
</div>


<!--v-->
# Collaborating with local and global R&D communities

![](images/logos/GSoC-Horizontal.svg)

* Blender add-on to create maps between UI signals and external controllers with libmapper
* Audio extension for the 3D scene exchange format glTF
* SATIE Multispatializer Mapper
* Using markerless motion capture for music generation, music creativity and music-dance interactions
* SATIE4Blender UX improvements
* **Accessibility** validation of user interfaces through continuous integration
* Explorable explanations for teaching digital arts concepts in hybrid telepresence campus
* Replace OpenGL by a multi-API rendering library in Splash
* Add support of SRT in Switcher, a multichannel low latency streamer 

https://summerofcode.withgoogle.com/programs/2023/organizations/society-for-arts-and-technology-sat

https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/

Notes:
highlight on AR/MR/VR/XR and EDI, example: accessibility automation for authoring tools

<!--v-->

<!-- .slide: id="end" -->
  <h2 data-i18n="title">Challenges and Opportunities for Equity Diversity and Inclusion in Immersive Arts</h2>
  <h3 data-i18n="conference"><a href="https://ivr2023.squarespace.com/">2023 Workshop on Inclusion in Virtual Reality</a></h3>
  <h4 data-i18n="subtitle">2023-03-06</h4>

  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name">
      <a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
      </div>
    </div>
    <div class="author">
      <img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
      <div class="name">
      <a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
      </div>
    </div>
  </div>


<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/1-LOGO_SAT_noir_sans_texte.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)
